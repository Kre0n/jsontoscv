const Json2csvParser = require('json2csv').Parser;
const fs = require('fs');

const data = fs.readFileSync('./parced002.text', {encoding: 'UTF-8'});
const json = JSON.parse(data);

try {
    const parser = new Json2csvParser();
    const csv = parser.parse(json);
    fs.writeFileSync('result.txt', csv);
} catch (err) {
    console.error(err);
}